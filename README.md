# Database Replication
## A - Overview
### 1 - What is database ?
- database là một bộ dữ liệu, thông tin , bản ghi được tổ chức ( dữ liệu được cấu trúc ) ⇒ có thể được truy cập và lưu trữ trong hệ thống máy tính thông qua DBMS ( phần mềm quản lý dữ liệu )

### 2 - Type of database
- Relational databases : lưu dữ liệu ở dạng cấu trúc (trong bảng có hàng và cột) sử dụng SQL để truy vấn dữ liệu ⇒ phù hợp để xử lý các mối qan hệ phức tạp giữa dữ liệu
- Non-relational databases : lưu trữ dữ liệu không có cấu trúc và linh hoạt ( sử dụng mô hình định hướng tài liệu, key-values, dựa trên biểu đồ) ⇒ dễ mở rộng hơn và xử lý lượng lớn dữ liệu không cấu trúc

### 3 - Some applied problems
- social media platform (hàng triệu người dùng , bài posts, photos, video) ; hệ thống phân tích khối lượng lớn dữ liệu real-time ⇒ Non-relational (mongodb)
- hệ thống banking (quản lý dữ liệu , thông tin người dùng) ; hệ thống quản lý nội dung (quản lý lượng lớn dữ liệu bán cấu trúc): relational database (mysql, postgresql)

### 4 - What is database replication ?
- database replication là quá trình sao chép dữ liệu từ cơ sở dữ liệu master sang một hoặc nhiều cơ sở dữ liệu slave nhằm cải thiện khả năng truy cập dữ liệu cũng như khả năng chịu lỗi và độ tin cậy của hệ thống.

### 5 - How does database replication ?
![alt text](images/image1.png)
- khi database master có sự thay đổi ⇒ dữ liệu sẽ được ghi vào file binlog (được ghi lại tất cả thay đổi của database structure (DDL) hoặc data (DML) từ lúc server khởi động bởi cùng một thread client thực hiện query) 
- luồng của master (dump thread : tạo các replica để lắng nghe thay đổi) ⇒ liên tục đọc binlog của master và gửi đến các replica ⇒ mỗi replica kết nối với resource sẽ yêu cầu một replica của binlog (replicate pull data từ resource chứ ko phải resource push data đến replica)
- ở side replica, IO thread nhận các thay đổi binlog được gửi bởi dump thread của master và được ghi vào file relay log
- ở side replica có thread khác gọi là SQL thread ⇒ liên tục đọc file relay log và ứng dụng các thay đổi cho server replica

### 6 - Timing data transfer
a - Synchronous
![alt text](images/image2.png)
- quá trình sync là quá trình ghi dữ liệu vào trong 2 hệ thống cùng thời gian
- ưu điểm : đảm bảo tính nhất quá của dữ liệu vì master sẽ ko commit thay đổi nếu replica xảy ra lỗi
- nhược điểm : cần nhiều thời gian để ghi dữ liệu vào database vì app server cần đợi feedback từ master và replica
- nếu có nhiều replica thì nó sẽ tốn nhiều thời gian hơn để sao chép => thời gian execution sẽ phụ thuộc vào số lượng replicas => Replication lag

b - Asynchronous
![alt text](images/image3.png)
- quá trình async là quá trình ghi dữ liệu vào trong 2 hệ thống tại thời điểm khác nhau
- đây là cách tiếp cận khác của quá trình đồng bộ => app server ko cần đợi dữ liệu được ghi vào tất cả database => khi dữ liệu đã được ghi vào master , app server có thể tiếp tục quá trình của nó
- ưu điểm : cải thiện hiệu suất của database
- nhược điểm : cần nhiều công sức để đảm bảo tính nhất quá của dữ liệu

## B - Master - Slave Replication
### 1 - How does Master - Slave replication work? 
![alt text](images/image4.png)
- Request ghi dữ liệu: Một request ghi dữ liệu (insert, update, hoặc delete) được tạo trên node master.
- Ghi dữ liệu cục bộ: Node master ghi dữ liệu vào cơ sở dữ liệu của nó.
- Sao chép dữ liệu: Node master sẽ sao chép dữ liệu đã ghi sang các node slave. Quá trình này thường liên quan đến việc truyền một binary log của thay đổi, đảm bảo việc truyền dữ liệu đầy đủ và hiệu quả.
- Áp dụng dữ liệu sao chép: Các node slave nhận dữ liệu sao chép và áp dụng nó vào cơ sở dữ liệu của chính chúng. Điều này duy trì sự đồng bộ giữa master và các slave.

### 2 - Config Master - Slave replication
a - install MySQL
```
    $ sudo apt update
    $ sudo apt-get install mysql-server mysql-client
```
b - config file /etc/mysql/mysql.conf.d/mysqld.cnf
```
    $ sudo vim /etc/mysql/mysql.conf.d/mysqld.cnf
```
- DB Master
```bash
    # dinh dang moi MySQL Server trong he thong
    server-id = 1
    
    # xac dinh ip MySQL se lang nghe
    bind-address = 192.168.188.140
    
    # xac dinh file binary log (ghi lai thay doi du lieu trong db)
    log_bin = /var/log/mysql/mysql-bin.log
    
    # xac dinh file index cho binary log (thong tin ve cac file binary log da duoc tao)
    log_bin_index = /var/log/mysql/mysql-bin.log.index
    
    # xac dinh file relay log (file chuyen tiep du lieu trong db)
    relay_log = /var/log/mysql/mysql-relay-bin
    
    # xac dinh file index cho relay log (thong tin ve cac file relay log da duoc tao)
    relay_log_index = /var/log/mysql/mysql-relay-bin.index
    
    # thoi gian file log duoc ton tai trong he thong
    expire_logs_days = 10
    
    # xac dinh dung luong toi da cua moi file binary log
    max_binlog_size = 100M
```
- DB Slave
```bash
    # dinh dang moi MySQL Server trong he thong
    server-id = 2
    
    # xac dinh ip MySQL se lang nghe
    bind-address = 192.168.188.141
    
    # xac dinh file binary log (ghi lai thay doi du lieu trong db)
    log_bin = /var/log/mysql/mysql-bin.log
    
    # xac dinh file index cho binary log (thong tin ve cac file binary log da duoc tao)
    log_bin_index = /var/log/mysql/mysql-bin.log.index
    
    # xac dinh file relay log (file chuyen tiep du lieu trong db)
    relay_log = /var/log/mysql/mysql-relay-bin
    
    
    # xac dinh file index cho relay log (thong tin ve cac file relay log da duoc tao)
    relay_log_index = /var/log/mysql/mysql-relay-bin.index
    
    # thoi gian file log duoc ton tai trong he thong
    expire_logs_days = 10
    
    # xac dinh dung luong toi da cua moi file binary log
    max_binlog_size = 100M
```
c - Restart MySQL
```
    $ sudo service mysql restart
    $ sudo service mysql status
```
d - login MySQL và tạo user và cấp quyền sao chép trên DBMaster
```
    $ sudo mysql -u root -p
    mysql> create user 'gota'@'%' identified with mysql_native_password by 'Abc!@#456';
    mysql> grant replication slave on *.* to 'gota'@'%';
    mysql> FLUSH PRIVILEGES;
```
e - sao chép dữ liệu giữa 2 server
- DB Master
```
    mysql> show master status\G
```
![alt text](images/image5.png)
- DB Slave
```
    mysql> stop slave;
    mysql> CHANGE MASTER TO MASTER_HOST='192.168.188.140',
    mysql> MASTER_USER='gota',
    mysql> MASTER_PASSWORD='Abc!@#456',
    mysql> MASTER_LOG_FILE='mysql-bin.000001',
    mysql> MASTER_LOG_POS=827;
    mysql> start slave;
```
```
    mysql> show slave status\G
```
![alt text](images/image6.png)
- Một số trường cần lưu ý
  + Slave_IO_State: Cho biết trạng thái hiện tại của luồng I/O. Trong trường hợp này, đó là "Đang chờ nguồn gửi sự kiện", nghĩa là nô lệ đang chờ sự kiện mới từ chủ
  + Master_Host, Master_User, Master_Port: Các tham số này hiển thị chi tiết về máy chủ chính mà nô lệ được kết nối.
  + Connect_Retry: Số giây mà máy phụ sẽ đợi trước khi cố gắng kết nối lại với máy chủ nếu kết nối không thành công.
  + Master_Log_File, Read_Master_Log_Pos: Các giá trị này thể hiện tên và vị trí tệp nhật ký nhị phân của máy chủ chính mà máy phụ hiện đang đọc.
  + Relay_Log_File, Relay_Log_Pos: Các giá trị này thể hiện tên và vị trí của tệp nhật ký chuyển tiếp trên máy phụ.
  + Slave_IO_Running, Slave_SQL_Running: Cho biết luồng I/O và luồng SQL có đang chạy hay không. "Có" cho cả hai nghĩa là bản sao đang hoạt động bình thường.
  + Replicate_Do_DB, Replicate_Ignore_DB, Replicate_Do_Table, Replicate_Ignore_Table: Các bộ lọc để bao gồm hoặc loại trừ các cơ sở dữ liệu hoặc bảng cụ thể khỏi quá trình sao chép.
  + Last_Errno, Last_Error: Số lỗi và thông báo cuối cùng nếu có.
  + Skip_Counter: Số sự kiện cần bỏ qua trước khi khởi động lại quá trình sao chép.
  + Exec_Master_Log_Pos: Vị trí trong tệp nhật ký nhị phân chính nơi luồng SQL hiện đang thực thi.
  + Relay_Log_Space: Tổng kích thước tính bằng byte của tất cả các tệp nhật ký chuyển tiếp.
  + Seconds_Behind_Master: Số giây mà bản sao chậm hơn bản gốc. 0 có nghĩa là bản sao đã được cập nhật.
  + Master_SSL_Allowed, Master_SSL_CA_File, Master_SSL_Cert: Thông tin liên quan đến SSL để kết nối an toàn.
  + Last_IO_Error, Last_SQL_Error: Thông tin về lỗi I/O hoặc SQL gần đây nhất gặp phải.
  + Master_Server_Id, Master_UUID: Mã định danh cho máy chủ chính.
  + Slave_SQL_Running_State: Trạng thái hiện tại của luồng SQL trên máy phụ.
  + Master_Retry_Count: Số lần luồng SQL nô lệ sẽ cố gắng kết nối lại với luồng chính.
  + Master_Bind: Địa chỉ cục bộ để liên kết kết nối với máy chủ.
  + Auto_Position: Cho biết liệu máy phụ có được định cấu hình để sử dụng bản sao dựa trên GTID hay không.
  + Retrieved_Gtid_Set, Executed_Gtid_Set: Thông tin GTID.
  + Replicate_Rewrite_DB: Viết lại tên cơ sở dữ liệu trong quá trình sao chép.
  + Channel_Name: Tên kênh sao chép.

### 3 - Test
- tạo database mới trên DBMaster
```
    mysql> create database test1;
```
- show database trên DMSlave
![alt text](images/image7.png)
- view file log : /var/log/mysql/mysql-bin.000001
![alt text](images/image8.png)

## C - Master - Master Replication
### 1 - How does Master - Master replication work? 
![alt text](images/image9.png)
- request ghi dữ liệu : một request ghi dữ liệu được tạo trên node master
- ghi dữ liệu cụ bộ : dữ liệu được ghi vào cơ sở dữ liệu trên node master
- sao chép dữ liệu : sau đó dữ liệu được sao chép sang node master khác
- xác nhận sao chép : node master đó xác nhận sao chép , đảm bảo các node có được dữ liệu ở phiên bản mới nhất được update
- nếu một node master down , node khác có thể tiếp tục phục vụ requests , đảm bảo hệ thống luôn sẵn sàng

### 2 - Config Master - Master replication
a - install MySQl
```
    $ sudo apt update
    $ sudo apt-get install mysql-server mysql-client
```
b - config file /etc/mysql/mysql.conf.d/mysqld.cnf
```
    $ sudo vim /etc/mysql/mysql.conf.d/mysqld.cnf
```
- DB Master 1
```bash
    # dinh dang moi MySQL Server trong he thong
    server-id = 1
    
    # xac dinh ip MySQL se lang nghe
    bind-address = 192.168.188.138
    
    report_host = dbmaster1
    
    # xac dinh file binary log (ghi lai thay doi du lieu trong db)
    log_bin = /var/log/mysql/mysql-bin.log
    
    # xac dinh dung luong toi da cua moi file binary log
    max_binlog_size = 100M
```
- DB Master 2
```bash
    # dinh dang moi MySQL Server trong he thong
    server-id = 2
    
    # xac dinh ip MySQL se lang nghe
    bind-address = 192.168.188.139
    
    report_host = dbmaster2
    
    # xac dinh file binary log (ghi lai thay doi du lieu trong db)
    log_bin = /var/log/mysql/mysql-bin.log
    
    # xac dinh dung luong toi da cua moi file binary log
    max_binlog_size = 100M
```
c - restart MySQL
```
    $ sudo service mysql restart
    $ sudo service mysql status
```
d - login MySQL và tạo user và cấp quyền sao chép
```
    $ sudo mysql -u root -p
    mysql> create user 'gota'@'%' identified with mysql_native_password by '123456';
    mysql> grant replication slave on *.* to 'gota'@'%';
```
e - sao chép dữ liệu giữa 2 server
- DB Master 1
```
    mysql> show master status\G
```
![alt text](images/image10.png)
- DB Master 2
```
    mysql> stop slave;
    mysql> CHANGE MASTER TO MASTER_HOST='192.168.188.138',
    mysql> MASTER_USER='gota',
    mysql> MASTER_PASSWORD='123456',
    mysql> MASTER_LOG_FILE='mysql-bin.000007',
    mysql> MASTER_LOG_POS=157;
    mysql> start slave;
```
```
    mysql> show slave status\G
```
![alt text](images/image11.png)
- DB Master 2
```
    mysql> show master status\G
```
![alt text](images/image12.png)
- DB Master 1
```
    mysql> stop slave;
    mysql> CHANGE MASTER TO MASTER_HOST='192.168.188.139',
    mysql> MASTER_USER='gota',
    mysql> MASTER_PASSWORD='123456',
    mysql> MASTER_LOG_FILE='mysql-bin.000007',
    mysql> MASTER_LOG_POS=157;
    mysql> start slave;
```
```
    mysql> show slave status;
```
![alt text](images/image13.png)
### 3 - Test
- tạo database mới bên DBMaster 1
```
    mysql> create database abcd;
```
- tạo tables mới bên DBMaster 1
```
    mysql> create table users (
        user_id INT PRIMARY KEY,
        username VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
    mysql> create table posts (
        post_id INT PRIMARY KEY,
        title VARCHAR(255) NOT NULL,
        content TEXT NOT NULL,
        user_id INT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        FOREIGN KEY (user_id) REFERENCES users(user_id)
    );
```
- show database trên DBMaster 2
![alt text](images/image14.png)
- show tables trên DBMaster 2
![alt text](images/image15.png)
- view file log : /var/log/mysql/mysql-bin.000007
![alt text](images/image16.png)
## References
- https://muhammad-rivaldy.medium.com/what-is-database-replication-and-how-the-databases-communications-each-other-a222d03ccced
- https://severalnines.com/resources/whitepapers/galera-cluster-mysql-tutorial